Example CMake project

Consists of:
Runner - executable
SimpleUnitTest - unit test made on googletest framework
MyStaticLibrary - static library
MyDynamicLibrary - shared library
ExternalLibraries - standalone CMake project with shared and static library

CMake parse CMakeLists.txt files and start build process only after all files was parsed. 
Commands add_library ; add_executable create isolated build targets, e.g. add_executable(SimpleTest ${SOURCES}) - means that target "SimpleTest" needs to be built from ${SOURCES} source files.

Example project has several add_library add_executable invocations which can be represented as following diagram

add_executable(SimpleTest ${SOURCES})
add_executable(Runner ${SOURCES})
add_library(MyStaticLibrary ${SOURCES})
add_library(MyDynamicLibrary SHARED ${SOURCES})

                                                                                                           
                 |---------------|              |----------------|                                          
                 |MyStaticLibrary|              |MyDynamicLibrary|                                        
                 |---------------|              |----------------|                                          
                                                                                                           
                                                                                                           
                 |------|                        |----------|                                                                  
                 |Runner|                        |SimpleTest|                                                 
                 |------|                        |----------|                                                                  
                                                                                                           


Command ExternalProject_Add performs build of another CMake project. It let's us split project on isolated standalone libraries. 

Example project contains separate CMake project ExternalLibraries and unit test framework googletest. 


                                                                                                           
                 |---------------|              |----------------|             |-----------------|     |----------|        
                 |MyStaticLibrary|              |MyDynamicLibrary|             |ExternalLibraries|     |googletest|      
                 |---------------|              |----------------|             |-----------------|     |----------|        
                         
                         
                         
                                                                                                           
                     |------|                     |----------|                                                                  
                     |Runner|                     |SimpleTest|                                                 
                     |------|                     |----------|                                                                  
                                                                                                           
Command include_directories add folders where compiler will search for header files. Scope of this command is limited by it's CMakeLists.txt file.

Command link_libraries tells compiler what libraries it need to link when build specific target. Scope of this command is limited by it's target.

link_libraries(SimpleTest googletest MyStaticLibrary MyDynamicLibrary ExternalLibrary) - target SimpleTest will depend on googletest, MyStaticLibrary, MyDynamicLibrary, ExternalLibrary targets. SimpleTest and Runner executables will be linked with libraries built for this targets.

link_libraries(SimpleTest googletest MyStaticLibrary MyDynamicLibrary ExternalLibrary)
link_libraries(Runner MyStaticLibrary MyDynamicLibrary ExternalLibrary)

                 |---------------|           |----------------|                       
                 |MyStaticLibrary|---\   /---|MyDynamicLibrary|                     
                 |---------------|    \ /    |----------------|                       
                         |             X               |
                         |            / \              |
                         |  /---------   ---------\    |
                         V V                       V   V                                                                                  
                     |------|                     |----------|       |----------|                                                               
                     |Runner|                     |SimpleTest|<------|googletest|                                              
                     |------|                     |----------|       |----------|                                                               
                         ^                             ^
                         |                             |
                         |                             |
                         |                             |
                 |-----------------|                   |
                 |ExternalLibraries|-------------------/
                 |-----------------|  
                 

Limited scope of link_libraries is a problem when we need to link library which depends on another library.

Example:
add_library(Library1 ${SOURCES})
add_library(Library2 ${SOURCES})
add_executable(Executable ${SOURCES})

link_libraries(Library2 Library1)
link_libraries(Executable Library2)

                 |--------|       |--------|                                                            
                 |Library1|       |Library2|                                                  
                 |--------|       |--------|                                                  
                     |                |                                              
                     V                V                                              
                 |--------|       |----------|                                                  
                 |Library2|       |Executable|                                                
                 |--------|       |----------|                                                  
                                                                                    
Executable will be linked only against Library2 and not against Library1. It applies to include_directories as well - headers needed to use Library1, Library2 will be not found by compiler.

There is target versions of link_libraries and include_directories commands - target_link_libraries, target_include_directories. This commands propagate target dependencies to it's dependees.

Example:
add_library(Library1 ${SOURCES})
add_library(Library2 ${SOURCES})
add_executable(Executable ${SOURCES})

target_link_libraries(Library2 Library1)
target_link_libraries(Executable Library2)

              |--------|           
              |Library1|           
              |--------|           
                  |                
                  V                
              |--------|           
              |Library2|           
              |--------|                         
                  |
                  V              
              |----------|
              |Executable|  
              |----------|  

Now Executable will inherit all linkage rules of Library2

ExternalProject_Add - gives us universal way to build some external source files. It even can automatically download sources from GitHub. We use simplest version of this command to build external CMake project.

ExternalProject_Add(ExternalLibrary_proj <...> )

It is not possible to simply link against ExternalLibrary_proj. CMake just doesn't know what to do with generic external project. User must specify exact paths to all library and header files compiled by external project.

To maintain uniform workflow with libraries built in main project we need to create dummy build target and manually specify libraries to link. Command add_library(ExternalLibrary INTERFACE) creates empty ExternalLibrary target which can be linked to another targets.

Linkage should occur only after ExternalLibrary_proj build and installation so we manually make ExternalLibrary dependant on ExternalLibrary_proj

add_dependencies(ExternalLibrary ExternalLibrary_proj)

Then we use propagating versions of link and include commands:

target_include_directories(ExternalLibrary INTERFACE ${EXTERNALLIBRARY_INSTALL_DIR}/include)
target_link_libraries(ExternalLibrary INTERFACE 
${EXTERNALLIBRARY_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}MyExternalStaticLibrary${CMAKE_STATIC_LIBRARY_SUFFIX})

