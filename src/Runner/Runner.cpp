#include "MyStaticLibrary.hpp"
#include "MyDynamicLibrary.hpp"
#include "MyExternalStaticLibrary.hpp"
#include "MyExternalDynamicLibrary.hpp"

#include<iostream>

int main() {

    double static_output = do_stuff_static(2.0);
    double dynamic_output = do_stuff_dynamic(2.0);
    double static_output_ext = do_stuff_static_ext(2.0);
    double dynamic_output_ext = do_stuff_dynamic_ext(2.0);

    std::cout << "Static library result: " << static_output << std::endl;
    std::cout << "Dynamic library result: " << dynamic_output << std::endl;
    std::cout << "External static library result: " << static_output_ext << std::endl;
    std::cout << "External dynamic library result: " << dynamic_output_ext << std::endl;

}