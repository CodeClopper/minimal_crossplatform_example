set(GOOGLETEST_INSTALL_DIR ${CMAKE_BINARY_DIR}/googletestInstallation)

# Command to compile googletest submodule source
# CMake will invoke "cmake ${SOURCE_DIR}" inside of BINARY_DIR folder
# and then make ; make install
# SOURCE_DIR - Path to googletest source
# INSTALL_DIR - Path where to install googletest library (This variable will be used in project subdirectories)
# BINARY_DIR - Path to folder for out-of-source build of googletest library
# CMAKE_ARGS - Actual designation of install folder
ExternalProject_Add(googletest_proj
SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/googletest/googletest 
INSTALL_DIR ${GOOGLETEST_INSTALL_DIR} 
BINARY_DIR ${CMAKE_BINARY_DIR}/googletestBuild
CMAKE_ARGS -DINSTALL_GTEST=1 -Dgtest_force_shared_crt=ON -DCMAKE_INSTALL_BINDIR=${GOOGLETEST_INSTALL_DIR} -DCMAKE_INSTALL_LIBDIR=${GOOGLETEST_INSTALL_DIR} -DCMAKE_INSTALL_INCLUDEDIR=${GOOGLETEST_INSTALL_DIR}
)

# dummy library target
add_library(googletest INTERFACE)

# Make googletest depend on googletest_proj so linkage of googletest 
# will occur after googletest_proj compilation and installation
add_dependencies(googletest googletest_proj)

# Add include folder to googletest target
target_include_directories(googletest INTERFACE ${GOOGLETEST_INSTALL_DIR})

# Set actual path to libraries to link
# On Windows google test library name differs on different build types
# On Linux we need to link pthread library in addition to google test
if (NOT UNIX)
	if(CMAKE_CONFIGURATION_TYPES STREQUAL "Debug")
		target_link_libraries(googletest INTERFACE ${GOOGLETEST_INSTALL_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}gtestd${CMAKE_STATIC_LIBRARY_SUFFIX} 
												   ${GOOGLETEST_INSTALL_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}gtest_maind${CMAKE_STATIC_LIBRARY_SUFFIX})
	else()
		target_link_libraries(googletest INTERFACE ${GOOGLETEST_INSTALL_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}gtest${CMAKE_STATIC_LIBRARY_SUFFIX} 
												   ${GOOGLETEST_INSTALL_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}gtest_main${CMAKE_STATIC_LIBRARY_SUFFIX})
	endif()
else()
target_link_libraries(googletest INTERFACE ${GOOGLETEST_INSTALL_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}gtest${CMAKE_STATIC_LIBRARY_SUFFIX} 
                                           ${GOOGLETEST_INSTALL_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}gtest_main${CMAKE_STATIC_LIBRARY_SUFFIX} 
                                           pthread )
endif()

