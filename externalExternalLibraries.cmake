set(EXTERNALLIBRARY_INSTALL_DIR ${CMAKE_BINARY_DIR}/ExternalLibrariesInstallation)

# Command to compile External Library source
# CMake will invoke "cmake ${SOURCE_DIR}" inside of BINARY_DIR folder
# and then make ; make install
# SOURCE_DIR - Path to External Library source
# INSTALL_DIR - Path where to install External library
# BINARY_DIR - Path to folder for out-of-source build of External library
# CMAKE_ARGS - Actual designation of install folder
ExternalProject_Add(ExternalLibrary_proj
SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/ExternalLibraries 
INSTALL_DIR ${EXTERNALLIBRARY_INSTALL_DIR} 
BINARY_DIR ${CMAKE_BINARY_DIR}/ExternalLibrariesBuild
BUILD_ALWAYS 1
CMAKE_ARGS -DCMAKE_INSTALL_BINDIR=${EXTERNALLIBRARY_INSTALL_DIR} -DCMAKE_INSTALL_LIBDIR=${EXTERNALLIBRARY_INSTALL_DIR} -DCMAKE_INSTALL_INCLUDEDIR=${EXTERNALLIBRARY_INSTALL_DIR} -DCMAKE_INSTALL_PREFIX=${EXTERNALLIBRARY_INSTALL_DIR}
)

# Dummy library target
add_library(ExternalLibrary INTERFACE)

# Custom target which will copy lib folder of external library
add_custom_target(ExternalLibraryCopyLib
                  COMMAND ${CMAKE_COMMAND} -E copy_directory ${EXTERNALLIBRARY_INSTALL_DIR}/lib/
				                                   ${CMAKE_BINARY_DIR}/bin/${CMAKE_CONFIGURATION_TYPES})

# Make ExternalLibraryCopyLib depend on ExternalLibrary_proj 
# so ExternalLibraryCopyLib target will be executed after ExternalLibrary_proj compilation and installation
add_dependencies(ExternalLibraryCopyLib ExternalLibrary_proj)

# Make ExternalLibrary depend on ExternalLibraryCopyLib so linkage of ExternalLibrary 
# will occur after copy
add_dependencies(ExternalLibrary ExternalLibraryCopyLib)

# Add include folder to ExternalLibrary target
target_include_directories(ExternalLibrary INTERFACE ${EXTERNALLIBRARY_INSTALL_DIR}/include)

# Set actual path to libraries to link
# On Windows we need to link .lib not .dll                  CMAKE_LINK_LIBRARY_SUFFIX = .lib    ;    CMAKE_SHARED_LIBRARY_SUFFIX = .dll
# On Linux we link .so                                      CMAKE_LINK_LIBRARY_SUFFIX =         ;    CMAKE_SHARED_LIBRARY_SUFFIX = .so
if (NOT UNIX)
target_link_libraries(ExternalLibrary INTERFACE ${EXTERNALLIBRARY_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}MyExternalStaticLibrary${CMAKE_STATIC_LIBRARY_SUFFIX}
                                      INTERFACE ${EXTERNALLIBRARY_INSTALL_DIR}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}MyExternalDynamicLibrary${CMAKE_LINK_LIBRARY_SUFFIX})
else()
target_link_libraries(ExternalLibrary INTERFACE ${EXTERNALLIBRARY_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}MyExternalStaticLibrary${CMAKE_STATIC_LIBRARY_SUFFIX}
                                      INTERFACE ${EXTERNALLIBRARY_INSTALL_DIR}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}MyExternalDynamicLibrary${CMAKE_SHARED_LIBRARY_SUFFIX})
endif()

# After build step copy library into common build folder
INSTALL( DIRECTORY ${EXTERNALLIBRARY_INSTALL_DIR} DESTINATION ${CMAKE_INSTALL_PREFIX} )
INSTALL( DIRECTORY ${EXTERNALLIBRARY_INSTALL_DIR}/lib DESTINATION ${CMAKE_INSTALL_PREFIX} )
INSTALL( DIRECTORY ${EXTERNALLIBRARY_INSTALL_DIR}/bin DESTINATION ${CMAKE_INSTALL_PREFIX} )