#include "gtest/gtest.h"
#include "test_environment.hpp"
/* this include turns the debug info on */
#include "ifdebug.hpp"

#include "MyStaticLibrary.hpp"
#include "MyDynamicLibrary.hpp"
#include "MyExternalStaticLibrary.hpp"
#include "MyExternalDynamicLibrary.hpp"

using namespace TestNamespace;

/* *********************************************************************** */
/* this macros launches the test with given parameters
/* *********************************************************************** */
TEST(Test, SmallTest ) {

    double static_test_val = do_stuff_static( 3.0 );
    double dynamic_test_val = do_stuff_dynamic( 3.0 );
    double static_output_val_ext = do_stuff_static_ext(3.0);
    double dynamic_output_val_ext = do_stuff_dynamic_ext(3.0);

    ASSERT_NEAR( static_test_val, 9.0, 0.0001 );
    ASSERT_NEAR( dynamic_test_val, 27.0, 0.0001 );
    ASSERT_NEAR( static_output_val_ext, -9.0, 0.0001 );
    ASSERT_NEAR( dynamic_output_val_ext, -27.0, 0.0001 );

    std::cout << "Static library test value: " << static_test_val << std::endl;
    std::cout << "Dynamic library test value: " << dynamic_test_val << std::endl;
    std::cout << "External static library value: " << static_output_val_ext << std::endl;
    std::cout << "External dynamic library value: " << dynamic_output_val_ext << std::endl;

}
